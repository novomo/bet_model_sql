const upload_error = require("../../../node_error_functions/upload_error")


module.exports = async (session, db) => {
  try{
  await db.createCollection("bets", {
    reuseExisting: true,
  });
} catch(err) {
  console.log(err)
  upload_error({
    errorTitle: "Creating/Editing Bets table",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}
};
